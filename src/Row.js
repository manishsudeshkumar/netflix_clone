// eslint-disable-next-line no-unused-vars
import react, { useState, useEffect } from 'react';
import axios from './axios';
import './Row.css';
import Youtube from 'react-youtube';
import movieTrailer from 'movie-trailer';

const Row = ({title, fetchUrl, isLarge, setOpen}) => {

    const [movies, setMovies] = useState([]);
    const [trailerURL, setTrailerURL] = useState()
    const imgUrl = 'https://image.tmdb.org/t/p/original/';

    useEffect(()=>{
        async function fetchMovies() {
            const request = await axios.get(fetchUrl);
            setMovies(request.data.results);
            return request;
        }
        fetchMovies();
    },[fetchUrl]);

    const opts={
        height: "390",
        width: '100%',
        playerVars: {
            autoplay: 1
        },
    }

    const handleMovieClick = (movie) => {
        if(trailerURL) {
            setTrailerURL('')
        }else{
            movieTrailer(movie?.title || movie?.name || movie?.original_name)
            .then((url) => {
                const urlParams = new URLSearchParams(new URL(url).search);
                setTrailerURL(urlParams.get('v'));
            })
            .catch((error) => {
                setOpen(true);
                console.log('error: ',error);
            });
        }
    };
    
    return (
        <div className="row">
            <h2>{title}</h2>
            <div className="posters" >
                {   movies.map(movie => (
                        <img 
                            key={movie.id} 
                            onClick = {() => handleMovieClick(movie)}
                            className={isLarge ? 'large_poster' : 'small_poster'}
                            src={imgUrl + `${isLarge ? movie.poster_path : movie.backdrop_path}`} 
                            alt={movie.title}
                        />
                    ))  
                } 
            </div>  
            { trailerURL && <Youtube videoId={trailerURL} opts={opts} /> }
        </div>
    )
};

export default Row;