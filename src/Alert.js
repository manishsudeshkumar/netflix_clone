import { Dialog, DialogContent, DialogActions, Button } from '@material-ui/core';
import { Warning } from '@material-ui/icons';
import './Alert.css'

function Alert(props) {

    const {
        open,
        handleClose
    } = props;

    return (
      <div>
        <Dialog
          open={open}
          onClose={handleClose}
        >
            <DialogContent className='content'>
                <div>
                     <Warning className='warning' style={{color: '#ffb74d'}} /> 
                </div>
                <div className='alertText'>
                     Trailer not available
                </div>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose} color='primary'>
                  OK
                </Button>   
            </DialogActions>
        </Dialog>
      </div>
    );
  }
  
  export default Alert;