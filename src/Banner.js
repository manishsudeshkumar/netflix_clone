import React, {useState, useEffect} from 'react';
import requests from './requests';
import axios from './axios';
import './Banner.css';

const Banner = () => {

  const [movie, setMovie] = useState();

  // The UseEffect hooks is to reload different banner image
  useEffect(() => {
    async function fetchData() {
        const { data } = await axios.get(requests.fetchNetflixOriginals);
        setMovie(
          data.results[
              (Math.floor(Math.random() * data.results.length-1))
          ]);
        return data;
    };
    fetchData();
  },[]);

  // This function limits the text of a movie on banner to given n number of words
  function truncate(str, n) {
      return str?.length > n ? str.substr(0, n-1) + "..." : str;
  }

  return (
    <header 
        className="banner"
        style={{
            backgroundSize: "cover",
            backgroundImage: `url(
                "https://image.tmdb.org/t/p/original/${movie?.backdrop_path}"
            )`,
            backgroundPosition: "center"
        }}
    >
        <div className="banner_contents">
            <h1 className="banner_title">{movie?.title || movie?.name || movie?.original_name}</h1>
            <div className="banner_buttons">
                <button className="banner_button">Play</button>
                <button className="banner_button">My List</button>
            </div>
            <h1 className="banner_description">
                {truncate(movie?.overview,150)}
            </h1>
        </div>
    <div className="banner_gradient"/>
    </header>
  );
}

export default Banner;