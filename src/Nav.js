import React, {useEffect, useState} from 'react';
import "./Nav.css";

const Nav = () => {
    const [show, setShow] = useState(false);
    
    useEffect(() => {
        window.addEventListener("scroll",() => {
            (window.scrollY > 100) ? setShow(true) : setShow(false);
        });
        return () => {
            window.removeEventListener("scroll");
        };
    }, [])
    return (
        <div className={`nav ${show && 'nav_black'}` }>
            <img 
                className="netflix_logo"
                alt="Netflix_logo"
                src="https://image.tmdb.org/t/p/original/wwemzKWzjKYJFfCeiB57q3r4Bcm.svg"
            />
            <img 
                className= "netflix_avatar"
                alt="my_profile"
                src="https://ih0.redbubble.net/image.618369215.1083/flat,1000x1000,075,f.u2.jpg"
            />
        </div>
    )
}

export default Nav
