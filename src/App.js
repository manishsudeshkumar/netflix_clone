import { useState } from 'react';
import './App.css';
import requests from './requests';
import Alert from './Alert';
import Nav from './Nav';
import Banner from './Banner';
import Row from './Row';

function App() {
 
  const [open, setOpen] = useState(false);
        
  const handleClose = () => {
    setOpen(false)
  };

  return (
    <div className="app">
      <Alert open={open} handleClose={handleClose}/>
      <Nav />
      <Banner />
      <Row title={'TOP RATED'} fetchUrl={requests.fetchTopRated} isLarge setOpen={setOpen} />
      <Row title={'NETFILX ORIGINALS'} fetchUrl={requests.fetchNetflixOriginals} setOpen={setOpen}/>
      <Row title={'ACTION MOVIES'} fetchUrl={requests.fetchActionMovies} setOpen={setOpen}/>
      <Row title={'COMEDY MOVIES'} fetchUrl={requests.fetchComedyMovies} setOpen={setOpen}/>
      <Row title={'HORROR MOVIES'} fetchUrl={requests.fetchHorrorMovies} setOpen={setOpen}/>
      <Row title={'ROMANTIC MOVIES'} fetchUrl={requests.fetchRomanceMovies} setOpen={setOpen}/>
      <Row title={'DOCUMENTARIES'} fetchUrl={requests.fetchDocumentaries} setOpen={setOpen}/>
    </div>
  );
}

export default App;
